package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func printPositiveEvenNums(str string) string {
	var positiveEvenNumbers strings.Builder

	for _, num := range strings.Split(str, ",") {
		n, err := strconv.Atoi(num)
		if err == nil {
			return err.Error()
		}
		if n > 0 && n%2 == 0 {
			if _, err := fmt.Fprintf(&positiveEvenNumbers, "%d ", n); err != nil {
				return err.Error()
			}
		}
	}
	return positiveEvenNumbers.String()
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	// when you hit enter - the data is read
	scanner.Scan()
	// this will read data from the scanner
	data := scanner.Text()

	fmt.Println(printPositiveEvenNums(data))
}
