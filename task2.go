package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
)

func split(s string) (string, error) {
	var newS string
	for _, v := range s {
		if string(v) != " " {
			if v >= '0' && v <= '9' {
				newS += string(v)
			} else {
				return "", errors.New("card must have only digits")
			}
		}
	}
	if len(newS) != 16 {
		return "", errors.New("card must have 16 digits")
	}
	return newS, nil
}

func checkBankCard(sInput string) string {
	s, err := split(sInput)

	if err != nil {
		return fmt.Sprintf("%s is invalid", sInput)
	}

	var sum int

	for i := len(s) - 1; i >= 0; i-- {
		number, _ := strconv.Atoi(string(s[i]))
		if i%2 == 0 {
			number *= 2
			if number > 9 {
				number -= 9
			}
			sum += number
		} else {
			sum += number
		}
	}

	if sum%10 == 0 {
		return fmt.Sprintf("%s is valid", sInput)
	}

	return fmt.Sprintf("%s is invalid", sInput)
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)

	scanner.Scan()

	input := scanner.Text()

	fmt.Println(checkBankCard(input))
}
