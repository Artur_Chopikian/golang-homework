package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type number struct {
	digits []int
}

func (n *number) splitEachDigit(input int) {
	n.digits = []int{}
	for i := input; i > 0; i /= 10 {
		n.digits = append(n.digits, i%10)
	}
}

func sum(nums []int) int {
	var result int
	for _, v := range nums {
		result += v
	}
	return result
}

func countLuckyTickets(min, max int) (int, int) {
	var simple int
	var hard int

	num := number{}

	for i := min; i <= max; i++ {
		num.splitEachDigit(i)
		// easy formula
		if sum(num.digits[:3]) == sum(num.digits[3:]) {
			simple++
		}
		// hard formula
		odd := 0
		even := 0
		for _, v := range num.digits {
			if v%2 == 0 {
				odd += v
			} else {
				even += v
			}
		}
		if odd == even {
			hard++
		}
	}
	return simple, hard
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	//when you hit enter - the data is read
	scanner.Scan()
	//this will read data from the scanner
	data := scanner.Text()

	input := strings.Split(data, " ")

	minNum, _ := strconv.Atoi(input[0])
	maxNum, _ := strconv.Atoi(input[1])

	simple, hard := countLuckyTickets(minNum, maxNum)

	fmt.Printf("EasyFormula: %v\nHardFormula: %v\n", simple, hard)
}
