package main

import (
	"bufio"
	"fmt"
	"os"
)

// -------------------------------------------------------------------------------------------------------------------
// Easy level:
func isPalindrome(s string) string {
	if len(s) < 1 {
		return fmt.Sprint("invalid input")
	}
	for i, j := 0, len(s)-1; i <= j; i, j = i+1, j-1 {
		if s[i] != s[j] {
			return fmt.Sprintf("%s is not palindrome", s)
		}
	}
	return fmt.Sprintf("%s is palindrome", s)
}

// -------------------------------------------------------------------------------------------------------------------
// Middle level:
func findFirstSubPalindrome(s string) string {
	var firstSubPalindromes string
	start := 0
	end := 0
	for c := 0; c < len(s); c++ { // c is center
		len1 := lenFromMid(s, c, c)
		len2 := lenFromMid(s, c, c+1)
		l := max(len1, len2)
		if l > end-start {
			start = c - (l-1)/2 // when l == 2, [i] remain unchanged, end plus + 1
			end = c + l/2       // when l is even num, [i] is not middle index, so [i] can not direct minus l/2, l has to minus one first.
			if l > 1 {
				firstSubPalindromes = s[start : end+1]
			}
		}
	}
	return firstSubPalindromes
}

// -------------------------------------------------------------------------------------------------------------------
// Hard level:
func findAllSubPalindromes(s string) []string {
	var subPalindromes []string
	start := 0
	end := 0
	for c := 0; c < len(s); c++ { // c is center
		len1 := lenFromMid(s, c, c)
		len2 := lenFromMid(s, c, c+1)
		l := max(len1, len2)
		if l > end-start {
			start = c - (l-1)/2 // when l == 2, [i] remain unchanged, end plus + 1
			end = c + l/2       // when l is even num, [i] is not middle index, so [i] can not direct minus l/2, l has to minus one first.
			if l > 1 {
				subPalindromes = append(subPalindromes, s[start:end+1])
			}
		}
	}
	return subPalindromes
}

func lenFromMid(s string, left, right int) int {
	for left >= 0 && right < len(s) && s[left] == s[right] {
		left--
		right++
	}
	return right - left - 1 // after left-- && right++, condition not met
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// -------------------------------------------------------------------------------------------------------------------
func main() {
	scanner := bufio.NewScanner(os.Stdin)
	//when you hit enter - the data is read
	scanner.Scan()
	//this will read data from the scanner
	data := scanner.Text()

	fmt.Println(isPalindrome(data))

	fmt.Println("Middle level: (print first subPalindrome)")
	fmt.Println(findFirstSubPalindrome(data))

	fmt.Println("Hard level: (print all subPalindromes)")
	fmt.Println(findAllSubPalindromes(data))
}
