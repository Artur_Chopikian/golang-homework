package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func fibonacci() func() int {
	first, second := 1, 1
	return func() int {
		ret := first
		first, second = second, first+second
		return ret
	}
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)

	scanner.Scan()

	strNumber := scanner.Text()

	number, err := strconv.Atoi(strNumber)
	if err != nil {
		return
	}

	f := fibonacci()
	for i := 0; i < number; i++ {
		fmt.Print(f(), " ")
	}
}
