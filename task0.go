package main

import (
	"fmt"
)

type chessboard struct {
	height int
	width  int
	char   string
}

func printChessboard(data chessboard) {
	for i := 0; i < data.height; i++ {
		for j := 0; j < data.width; j++ {
			if j%2 == i%2 {
				fmt.Print(data.char)
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}

func main() {
	var data chessboard

	if _, err := fmt.Scan(&data.height, &data.width, &data.char); err != nil {
		return
	}

	printChessboard(data)
}
